# Lancer Battlegroup NPC Data - Lancer System, Foundry VTT

In this module are the NPCs from the Lancer Battlegroup core rulebook, along with all of their abilities, weapons, templates, flagships, escorts, aces, and deployables to represent payloads and wings.

This module is not an official Lancer product; it is a third party work, and is not affiliated with Massif Press. This module is published via the Lancer Third Party License.

Lancer is copyright Massif Press

Tokens by the talented Suki, found here: https://sukirpg.itch.io/battlegroup-tokens

# Useage: 

Once you have added the module to your module list in Foundry, start a new world. Be sure to download and select the LANCER system for that world.

![1](https://i.imgur.com/fXKdZr8.jpg)

You must then load your new world and select the Battlegroup NPCs module and enable it.

![2](https://i.imgur.com/QRyBjvm.jpg)
![3](https://i.imgur.com/nsYkFEw.jpg)

This should create 9 new Compendiums, for each, right click and select "Import All Content". 

![4](https://i.imgur.com/3KZ2mrg.jpg)
![5](https://i.imgur.com/F8axhu7.jpg)

Ensure that "Keep Document IDs" is checked.

![6](https://i.imgur.com/PfSCS8z.jpg)

Once you have imported all the compendiums, you should see in your Actors tab all of the tokens for ships, payload/wings, and so on organized by category. 

The Templates will be in Items, and you may add in templates by simply dragging them onto a Token or Actor.

Dragging Actors onto the map should create a token copy, by defaul Flagships are size 4, Escorts are size 2, and Payload/wings are Size 1, so the size differences should be preserved.

Tokens will have HP displayed, and names displayable if you hover over them. All players will by default see HP and names, but not the token's statistics or abilities.


